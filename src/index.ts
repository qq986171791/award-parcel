/**
 * Created by user on 2018/3/12.
 */
import './main.scss';
import $ = require('jquery');
import {
    PageAwardHome,
    PageAwardLogin,
    PageAwardReadme,
    PageAwardQuan,
    PageAwardMessage,
    PageAwardArrow,
    PageAwardIntercept,
    PageAwardLoding
} from './Component'
var pageAwardLoding = null;

$(function () {
    pageAwardLoding = new PageAwardLoding();
    var pageAwardHome = new PageAwardHome();
    var pageAwardLogin = new PageAwardLogin();
    var pageAwardReadme = new PageAwardReadme();
    var pageAwardQuan = new PageAwardQuan();
    var pageAwardMessage = new PageAwardMessage();
    var pageAwardArrow = new PageAwardArrow();
    var pageAwardIntercept = new PageAwardIntercept();
});

window.onload = function () {
    pageAwardLoding.hide();
}