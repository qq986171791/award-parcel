const $ = require('jquery');
import bus from '../assets/js/bus';
import config from '../assets/js/config';
import {userService,wxService,awardService,activityService} from '../assets/js/service';
import JqueryRotate from '../assets/lib/jqueryRotate';
JqueryRotate($);
export var PageAwardHome = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-home'), // 接收DOM对象
            scaleWidth:null,//比例尺 for 宽度
            scaleHeight:null,//比例尺 for 高度
            rotating:false, //是否正在旋转
            activityId:config.activityId,
            awardReady:{ready:false,code:null}, //奖品是否准备好
            pos:{ //position and size

            },
            url:{
                getAwardTimes:'rest/lottery/getUserDegree' //获取抽奖次数
            },
            dom:{
                domPoint:'dom-point', //指针
                domRotate:'dom-rotate',//大转盘
                domReadme:'dom-readme',//攻略
                domTimes:'dom-times', //n次抽奖机会
                domGain:'dom-gain', //屋顶能赚钱
                domPlan:'dom-plan', //0元用电计划
                domTel:'dom-tel', //用电计划
                domMessage:'dom-message',//留言
                domGift:'dom-gift', //礼物
                domLogout:'dom-logout', //登出
                domAward:'dom-award', //奖品
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                this._initDom();
                if(_publish.utils.isLogin()){
                    //初始化次数
                    _publish._initDomTimes();
                }
                _publish._initScale();
                _publish._initDomStyle();
                this._initListeners();
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _initDomStyle:function () {
                var scaleWidth = this.scaleWidth,
                    scaleHeight = this.scaleHeight;
                //初始化大转盘 样式
                $(_publish.dom.domRotate).css({
                    top:scaleHeight * 1010 + 'px',
                    left:scaleWidth * 360 + 'px',
                    width:scaleWidth * 1245 + 'px'
                });
                //初始化指针样式
                $(_publish.dom.domPoint).css({
                    top:scaleHeight * 1175 + 'px',
                    left:scaleWidth * 740 + 'px',
                    width:scaleWidth * 500 + 'px'
                });
                //初始化攻略样式
                $(_publish.dom.domReadme).css({
                    top:scaleHeight * 520 + 'px',
                    left:scaleWidth * 1180 + 'px',
                    width:scaleWidth * 400 + 'px',
                    height:scaleHeight * 150 + 'px',
                });
                //初始化次数样式
                $(_publish.dom.domTimes).css({
                    top:scaleHeight * 765 + 'px',
                    left:scaleWidth * 875 + 'px'
                });
                //初始化赚钱样式
                $(_publish.dom.domGain).css({
                    top:scaleHeight * 2710 + 'px',
                    left:scaleWidth * 290 + 'px',
                    width:scaleWidth * 1400 + 'px',
                    height:scaleHeight * 120 + 'px',
                });
                //初始化用电计划样式
                $(_publish.dom.domPlan).css({
                    top:scaleHeight * 2860 + 'px',
                    left:scaleWidth * 290 + 'px',
                    width:scaleWidth * 1400 + 'px',
                    height:scaleHeight * 120 + 'px',
                });
                //初始化客服电话样式
                $(_publish.dom.domTel).css({
                    top:scaleHeight * 3438 + 'px',
                    left:scaleWidth * 330 + 'px',
                    width:scaleWidth * 670 + 'px',
                    height:scaleHeight * 100 + 'px',
                });
                //初始化留言样式
                $(_publish.dom.domMessage).css({
                    top:scaleHeight * 3438 + 'px',
                    left:scaleWidth * 1020 + 'px',
                    width:scaleWidth * 670 + 'px',
                    height:scaleHeight * 100 + 'px',
                });
                //初始化礼物样式
                $(_publish.dom.domGift).css({
                    top:scaleHeight * 1600 + 'px',
                    left:scaleWidth * 200 + 'px',
                    width:scaleWidth * 670 + 'px',
                });
                //初始化退出样式
                $(_publish.dom.domLogout).css({
                    top:scaleHeight * 900 + 'px',
                    left:scaleWidth * 200 + 'px',
                    width:scaleWidth * 670 + 'px',
                });
                //初始化奖品样式
                $(_publish.dom.domAward).css({
                    top:scaleHeight * 900 + 'px',
                    left:scaleWidth * 1500 + 'px',
                });
            },
            /**
             * 初始化比例尺
             *
             */
            _initScale:function () {
                var mapWidth = this.element.clientWidth,
                    mapHeight = this.element.clientHeight;
                this.scaleWidth = mapWidth / 1920;
                this.scaleHeight = mapWidth / 1080;
            },
            /**
             * 减抽奖次数
             *
             */
            _subDomTimes:function () {
                var num = $(_publish.dom.domTimes).text();
                $(_publish.dom.domTimes).text(num-1);
            },
            /**
             * 初始化抽奖次数
             *
             */
            _initDomTimes:function () {
                $.post(_publish.url.getAwardTimes,{id:_publish.activityId,recommendUserId:_publish.utils.getQueryString('recommendUserId')},function (json) {
                    if(1 == json.status){
                        $(_publish.dom.domTimes).text(json.result);
                    }
                });
            },
            /**
             * 旋转到 某 角度
             *
             * @params b {boolean} true | false
             */
            setRotating:function (b) {
                _publish.rotating = b;
            },
            /**
             * 设置奖品是否已准备好
             *
             * @params obj {object} {ready:true | false,code:0~9}
             */
            setAwardReady:function (obj) {
                _publish.awardReady = obj;
            },
            /**
             * 抽奖
             *
             */
            _rndAward:function (callback) {
                $.post('rest/lottery/main',{id:_publish.activityId},function(json){
                    if(/异常/.test(json.message)){
                        alert('网络开小车了');
                        return;
                    }
                    if(0 == json.status){
                        alert(json.message);
                        return;
                    }
                    if(1 == json.status){
                        callback(json);
                    }
                });
            },
            /**
             * 旋转到 某 角度
             *
             * @params code {integer} 奖品代码
             * @params angles {integer} 角度
             * @params result {string} 旋转结果
             */
            _rotateTo:function (code,angles,result) {
                _publish.setRotating(!_publish.rotating);
                $(_publish.dom.domRotate).stopRotate();
                $(_publish.dom.domRotate).rotate({
                    angle:0,
                    animateTo:angles+1800,
                    duration:5000,
                    callback:function (){
                        //set award
                        bus.trigger('pageAwardQuan/_publish',{
                            pageAwardQuan:{
                                fun:'_setAward',
                                args:[angles,result]
                            }
                        });
                        //show award
                        bus.trigger('pageAwardQuan/_publish',{
                            pageAwardQuan:{
                                fun:'_togglePage',
                                args:[true]
                            }
                        });
                        _publish.setRotating(!_publish.rotating);
                    }
                });
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.dom.domReadme).on('click',function () {
                    bus.trigger('pageAwardReadme/_publish',{
                        pageAwardReadme:{
                            fun:'_togglePage',
                            args:[true]
                        }
                    });
                });
                $(_publish.dom.domPoint).on('click',function () {
                    if(_publish.utils.isRotating()){return;}
                    if(_publish.utils.isNoneAward()){
                        bus.trigger('pageAwardReadme/_publish',{
                            pageAwardReadme:{
                                fun:'_togglePage',
                                args:[true]
                            }
                        });
                        return;
                    }
                    _publish._rndAward(function (json) {
                        var item = json.result.attrId;
                        var strategy = {
                            14:function () {
                                return [14,80,'7.7折扣+无利润'];
                            },
                            15:function () {
                                return [15,115,'3800智能配电箱'];
                            },
                            16:function () {
                                return [16,150,'1800摄像头'];
                            },
                            17:function () {
                                return [17, 190, '20话费'];
                            },
                            18:function () {
                                return [18, 230, '5000只能充电桩+储能设备'];
                            },
                            19:function () {
                                return [19, 260, '800空气闹钟'];
                            },
                            20:function () {
                                return [20, 300, '50话费'];
                            },
                            21:function () {
                                return [21, 340, '6800多功能空气净化器'];
                            },
                            22:function () {
                                return [22, 380, '50000星光之家+光伏屋顶'];
                            },
                            23:function () {
                                return [23, 415, '5话费   '];
                            }
                        };
                        var args = strategy[item]();
                        _publish._rotateTo.apply(_publish,args);
                        //减次数
                        _publish._subDomTimes();
                    });
                });
                bus.on('pageAwardHome/msgPointer',function () {
                    $(_publish.dom.domPoint).trigger('click');
                });
                bus.on('pageAwardHome/_publish',function (e) {
                    var pageAwardHome = e.pageAwardHome,
                        fun = pageAwardHome.fun,
                        args = pageAwardHome.args;
                    _publish[fun].apply(_publish,args);
                });
                $(_publish.dom.domMessage).on('click',function () {
                    bus.trigger('pageAwardMessage/_publish',{
                        pageAwardMessage:{
                            fun:'_togglePage',
                            args:[true]
                        }
                    });
                });
                $(_publish.dom.domAward).on('click',function () {
                    if(!_publish.utils.isLogin()){
                        event.preventDefault();
                        alert('请先登录');
                    }
                });
                $(_publish.dom.domLogout).on('click',function () {
                    localStorage.clear();
                    location.reload();
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                delegates: function(element, configs){
                    var el = $(element);
                    for (var name in configs) {
                        var value = configs[name];
                        if (typeof value === 'function') {
                            var obj = {};
                            obj.click = value;
                            value = obj;
                        }
                        for (var type in value) {
                            el.delegate(name, type, value[type]);
                        }
                    }
                },
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                },
                //是否已经准备好奖品
                isAwardReady:function () {
                    return _publish.awardReady.ready;
                },
                //获取queryString
                getQueryString:function (name) {
                    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null) {
                        return unescape(r[2]);
                    }
                    return null;
                },
                //抽奖次数是否为空
                isNoneAward:function () {
                    if($(_publish.dom.domTimes).text() == '0'){
                        return true;
                    }
                    return false;
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardLogin = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-login'), // 接收DOM对象
            scaleWidth:null,//比例尺 for 宽度
            scaleHeight:null,//比例尺 for 高度
            rotating:false, //是否正在旋转
            user:null,
            pos:{ //position and size

            },
            url:{
                login:'userLogin',
                sendCode:'rest/auth/sendVerificationCode',
                checkCode:'rest/auth/checkVerificationCode'
            },
            dom:{
                domPhone:'dom-phone', //手机号
                domCode:'dom-code', //验证码
                domSend:'dom-send', //发送验证码
                domSubmit:'dom-submit' //马上抽奖
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                activityService.initActivityInfo(function (json) {
                    if(!json || !json.result){
                        console.log('警告信息',json);
                        return;
                    }
                    if(json.result.activityErrorStatus){
                        //拦截
                        bus.trigger('pageAwardIntercept/_publish',{
                            pageAwardIntercept:{
                                fun:'_togglePage',
                                args:[true]
                            }
                        });
                        _publish._togglePage(false);
                        return;
                    }
                    if(_publish.utils.isLogin()){
                        config.c('activityId',json.result.activity.id);
                        _publish._togglePage(false);
                        userService.initUserInfo(function () {
                            //注册分享
                            wxService.initShare({
                                title: '星光之家十一抽奖活动!',
                                link: _url_webchart_page +'award/home.html',
                                imgUrl: 'https://www.xingtown.com/wechat/xg-wechat/img/background/star.png', // 分享图标
                                success: function () {
                                    awardService.addAwardTimes('share');
                                },
                                cancel: function () {

                                },
                                recommendUserId:userService.getUserInfo().id
                            });
                        });
                        return;
                    }
                    _publish._togglePage(true);
                    _publish._initDom();
                    _publish._initScale();
                    _publish._initDomStyle();
                    _publish._initListeners();
                });
            },
            /**
             * 发送验证码
             */
            _sendVefifyCode:function () {
                $.post(_publish.url.sendCode,{mobile:$(_publish.dom.domPhone).val()},function (json) {
                    alert(json.message);
                });
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow-y','hidden');
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _initDomStyle:function () {
                var scaleWidth = this.scaleWidth,
                    scaleHeight = this.scaleHeight;
                //初始化手机号码样式
                $(_publish.dom.domPhone).css({
                    top:scaleHeight * 1005 + 'px',
                    left:scaleWidth * 350 + 'px',
                    width:scaleWidth * 805 + 'px',
                    height:scaleHeight * 75 + 'px'
                });
                //初始化发送验证码样式
                $(_publish.dom.domSend).css({
                    top:scaleHeight * 1022 + 'px',
                    left:scaleWidth * 1180 + 'px',
                    width:scaleWidth * 405 + 'px',
                    height:scaleHeight * 55 + 'px'
                });
                //初始化验证码样式
                $(_publish.dom.domCode).css({
                    top:scaleHeight * 1142 + 'px',
                    left:scaleWidth * 350 + 'px',
                    width:scaleWidth * 1205 + 'px',
                    height:scaleHeight * 75 + 'px'
                });
                //初始化马上抽奖样式
                $(_publish.dom.domSubmit).css({
                    top:scaleHeight * 1263 + 'px',
                    left:scaleWidth * 350 + 'px',
                    width:scaleWidth * 1205 + 'px',
                    height:scaleHeight * 90 + 'px'
                });
            },
            /**
             * 验证手机号
             *
             */
            _validatePhone:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    return false;
                }
                return true;
            },
            /**
             * 验证手机验证码
             *
             */
            _validateCode:function (callback) {
                $.post(_publish.url.checkCode,{mobile:$(_publish.dom.domPhone).val(),verificationCode:$(_publish.dom.domCode).val()},function (json) {
                    if(json.success){
                        callback();
                    }else{
                        alert(json.message);
                    }
                });
            },
            /**
             * 验证表单
             *
             */
            _validateForm:function () {
                if(!_publish._validatePhone()){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 登录
             */
            _login:function () {
                $.post(_publish.url.login,{
                    username:$(_publish.dom.domPhone).val(),
                    password:$(_publish.dom.domCode).val(),
                    loginWay:"vercode",
                    userType:'ownerUser',
                    recommendUser:_publish.utils.getQueryString('recommendUserId')
                },function (json) {
                    if(1 == json.status){
                        //登录获取token 把token 存入 localStorage
                        localStorage.token = json.result;
                        _publish._togglePage(false);
                        //初始化用户信息
                        userService.initUserInfo(function () {
                            //注册分享
                            wxService.initShare({
                                title: '星光之家十一抽奖活动!',
                                link: _url_webchart_page +'award/home.html',
                                imgUrl: 'https://www.xingtown.com/wechat/xg-wechat/img/background/star.png', // 分享图标
                                success: function () {
                                    awardService.addAwardTimes('share');
                                },
                                cancel: function () {

                                },
                                recommendUserId:userService.getUserInfo().id
                            });
                            //初始化登录次数
                            bus.trigger('pageAwardHome/_publish',{
                                pageAwardHome:{
                                    fun:'_initDomTimes',
                                    args:[]
                                }
                            });
                        });
                    }else{
                        alert(json.message);
                    }
                });
            },
            /**
             * 初始化比例尺
             *
             */
            _initScale:function () {
                var mapWidth = this.element.clientWidth,
                    mapHeight = this.element.clientHeight;
                this.scaleWidth = mapWidth / 1920;
                this.scaleHeight = mapWidth / 1080;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.dom.domSend).on('click',function () {
                    if(!_publish._validatePhone()){
                        alert("手机号有误，请充填！");
                        return;
                    }
                    _publish._sendVefifyCode();
                });
                $(_publish.dom.domSubmit).on('click',function () {
                    if(_publish._validateForm()){
                        _publish._validateCode(function () {
                            _publish._login();
                        });
                    }
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                delegates: function(element, configs){
                    var el = $(element);
                    for (var name in configs) {
                        var value = configs[name];
                        if (typeof value === 'function') {
                            var obj = {};
                            obj.click = value;
                            value = obj;
                        }
                        for (var type in value) {
                            el.delegate(name, type, value[type]);
                        }
                    }
                },
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                },
                //获取queryString
                getQueryString:function (name) {
                    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null) {
                        return unescape(r[2]);
                    }
                    return "";
                },
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardReadme = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-readme'), // 接收DOM对象
            scaleWidth:null,//比例尺 for 宽度
            scaleHeight:null,//比例尺 for 高度
            rotating:false, //是否正在旋转
            pos:{ //position and size

            },
            dom:{
                domClose:'dom-close',
                domInvite:'dom-invite'
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                _publish._togglePage(false);
                this._initDom();
                _publish._initScale();
                _publish._initDomStyle();
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow-y','hidden');
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _initDomStyle:function () {
                var scaleWidth = this.scaleWidth,
                    scaleHeight = this.scaleHeight;
                //初始化关闭样式
                $(_publish.dom.domClose).css({
                    top:scaleHeight * 552 + 'px',
                    left:scaleWidth * 1640 + 'px',
                    width:scaleWidth * 160 + 'px',
                    height:scaleHeight * 95 + 'px'
                });
                //初始化邀请样式
                $(_publish.dom.domInvite).css({
                    top:scaleHeight * 1115 + 'px',
                    left:scaleWidth * 800 + 'px',
                    width:scaleWidth * 615 + 'px',
                    height:scaleHeight * 75 + 'px'
                });
            },
            /**
             * 初始化dom样式
             *
             */
            _validateForm:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 初始化比例尺
             *
             */
            _initScale:function () {
                var mapWidth = this.element.clientWidth,
                    mapHeight = this.element.clientHeight;
                this.scaleWidth = mapWidth / 1920;
                this.scaleHeight = mapWidth / 1080;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.dom.domClose).on('click',function () {
                    _publish._togglePage(false);
                });
                $(_publish.dom.domInvite).on('click',function () {
                    bus.trigger('pageAwardArrow/_publish',{
                        pageAwardArrow:{
                            fun:'_togglePage',
                            args:[true]
                        }
                    });
                    _publish._togglePage(false);
                });
                bus.on('pageAwardReadme/_publish',function (e) {
                    var pageAwardReadme = e.pageAwardReadme,
                        fun = pageAwardReadme.fun,
                        args = pageAwardReadme.args;
                    _publish[fun].apply(_publish,args);
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                delegates: function(element, configs){
                    var el = $(element);
                    for (var name in configs) {
                        var value = configs[name];
                        if (typeof value === 'function') {
                            var obj = {};
                            obj.click = value;
                            value = obj;
                        }
                        for (var type in value) {
                            el.delegate(name, type, value[type]);
                        }
                    }
                },
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardQuan = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-quan'), // 接收DOM对象
            scaleWidth:null,//比例尺 for 宽度
            scaleHeight:null,//比例尺 for 高度
            rotating:false, //是否正在旋转
            pos:{ //position and size

            },
            dom:{
                domClose:'dom-close',
                domAgain:'dom-again',
                domUse:'dom-use',
                domBg:'dom-bg'
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                _publish._togglePage(false);
                this._initDom();
                _publish._initScale();
                _publish._initDomStyle();
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow','hidden');
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _setAward:function (angles) {
                var url = $(_publish.dom.domBg).attr('dom-bg').replace(/\{\{angles\}\}/,angles);
                $(_publish.dom.domBg).attr('src',url);
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _initDomStyle:function () {
                var scaleWidth = this.scaleWidth,
                    scaleHeight = this.scaleHeight;
                //初始化关闭样式
                $(_publish.dom.domClose).css({
                    top:scaleHeight * 262 + 'px',
                    left:scaleWidth * 1520 + 'px',
                    width:scaleWidth * 167 + 'px',
                    height:scaleHeight * 95 + 'px'
                });
                //初始化再次使用样式
                $(_publish.dom.domAgain).css({
                    top:scaleHeight * 1365 + 'px',
                    left:scaleWidth * 360 + 'px',
                    width:scaleWidth * 1215 + 'px',
                    height:scaleHeight * 120 + 'px'
                });
                //初始化马上使用样式
                $(_publish.dom.domUse).css({
                    top:scaleHeight * 1545 + 'px',
                    left:scaleWidth * 360 + 'px',
                    width:scaleWidth * 1215 + 'px',
                    height:scaleHeight * 120 + 'px'
                });
            },
            /**
             * 初始化dom样式
             *
             */
            _validateForm:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 初始化比例尺
             *
             */
            _initScale:function () {
                var mapWidth = this.element.clientWidth,
                    mapHeight = this.element.clientHeight;
                this.scaleWidth = mapWidth / 1920;
                this.scaleHeight = mapWidth / 1080;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.dom.domClose).on('click',function () {
                    _publish._togglePage(false);
                });
                $(_publish.dom.domInvite).on('click',function () {
                    //邀请好友一起玩
                    alert("邀请好友一起玩");
                });
                $(_publish.dom.domAgain).on('click',function () {
                    bus.trigger('pageAwardHome/msgPointer');
                    _publish._togglePage(false);
                });
                $(_publish.dom.domUse).on('click',function () {
                    location.href = 'quan.html';
                });
                bus.on('pageAwardQuan/_publish',function (e) {
                    var pageAwardQuan = e.pageAwardQuan,
                        fun = pageAwardQuan.fun,
                        args = pageAwardQuan.args;
                    _publish[fun].apply(_publish,args);
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                delegates: function(element, configs){
                    var el = $(element);
                    for (var name in configs) {
                        var value = configs[name];
                        if (typeof value === 'function') {
                            var obj = {};
                            obj.click = value;
                            value = obj;
                        }
                        for (var type in value) {
                            el.delegate(name, type, value[type]);
                        }
                    }
                },
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardMessage = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-message'), // 接收DOM对象
            pos:{ //position and size

            },
            dom:{
                domMessage:'dom-message',
                domSubmit:'dom-submit'
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                _publish._togglePage(false);
                this._initDom();
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow','hidden');
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _validateForm:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.dom.domSubmit).on('click',function () {
                    event.stopPropagation();
                    $.post('rest/feedback/saveFeedbackInfo',{
                        feedbackContent:$(_publish.dom.domMessage).val(),
                        feedbackFrom: "award"
                    },function (json) {
                        if(1 == json.status){
                            alert(json.message);
                            _publish._togglePage(false);
                        }
                    });
                });
                $(_publish.element).on('click',function () {
                    _publish._togglePage(false);
                });
                $(_publish.dom.domMessage).on('click',function () {
                    event.stopPropagation();
                });
                bus.on('pageAwardMessage/_publish',function (e) {
                    var pageAwardMessage = e.pageAwardMessage,
                        fun = pageAwardMessage.fun,
                        args = pageAwardMessage.args;
                    _publish[fun].apply(fun,args);
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardArrow = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-arrow'), // 接收DOM对象
            pos:{ //position and size

            },
            dom:{

            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                _publish._togglePage(false);
                this._initDom();
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow-y','hidden');
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _validateForm:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                $(_publish.element).on('click',function () {
                    _publish._togglePage(false);
                });
                bus.on('pageAwardArrow/_publish',function (e) {
                    var pageAwardArrow = e.pageAwardArrow,
                        fun = pageAwardArrow.fun,
                        args = pageAwardArrow.args;
                    _publish[fun].apply(fun,args);
                });
            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardIntercept = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-intercept'), // 接收DOM对象
            pos:{ //position and size

            },
            dom:{
                domTip:'dom-tip'
            },
            activityId:config.activityId,
            tip:null,//提示信息
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                _publish._togglePage(false);
                this._initDom();
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(_publish.element).css('z-index',10);
                    $('body').css('overflow-y','hidden');
                    $(_publish.dom.domTip).text(activityService.getActivityInfo().result.activityErrorStatus);
                }else{
                    $(_publish.element).css('z-index',-1);
                    $('body').css('overflow-y','visible');
                }
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _validateForm:function () {
                if(!(/^1[34578]\d{9}$/.test($(_publish.dom.domPhone).val()))){
                    alert("手机号码有误，请重填");
                    return false;
                }
                if(!$(_publish.dom.domCode).val()){
                    alert("请填写正确的验证码！");
                    return false;
                }
                return true;
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){
                bus.on('pageAwardIntercept/_publish',function (e) {
                    var pageAwardIntercept = e.pageAwardIntercept,
                        fun = pageAwardIntercept.fun,
                        args = pageAwardIntercept.args;
                    _publish[fun].apply(_publish,args);
                });

            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();
export var PageAwardLoding = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-loading'), // 接收DOM对象
            pos:{ //position and size

            },
            dom:{
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                this._initListeners();
            },
            /**
             * 显示或隐藏当前页面
             * @params b {boolean} true | false
             */
            _togglePage:function (b) {
                if(b){
                    $(this.element).show();
                }else{
                    $(this.element).hide(1000);
                }
            },
            hide(){
                this._togglePage(false);
            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){

            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();