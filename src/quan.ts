/**
 * Created by user on 2018/3/13.
 */
import './quan.scss';
const $ = require('jquery');
import Auth from './assets/lib/auth';

Auth($);

var quanService = (function () {
    var url = {
        getQuanList:'rest/coupon/getMyList'
    };
    var quanList = null;
    return {
        initQuanList:function (activityId,callback) {
            $.post(url.getQuanList,{id:activityId},function(json){
                quanList = json.result;
                callback(quanList);
            });
        },
        getQuanList:function () {
            return quanList;
        }
    }
})();
var PageAwardQuan = (function () {
    function _initPublish(options){
        // 默认参数对象
        var defaults = {
            element: document.getElementById('page-award-quan'), // 接收DOM对象
            scaleWidth:null,//比例尺 for 宽度
            scaleHeight:null,//比例尺 for 高度
            activityId:7,
            dom:{
                dom1800w:'dom-1800w', //未使用
                dom1800u:'dom-1800u', //已使用
                dom1800g:'dom-1800g', //已过期

                dom20w:'dom-20w', //未使用
                dom20u:'dom-20u', //已使用
                dom20g:'dom-20g', //已过期

                dom3800w:'dom-3800w', //未使用
                dom3800u:'dom-3800u', //已使用
                dom3800g:'dom-3800g', //已过期

                dom5w:'dom-5w', //未使用
                dom5u:'dom-5u', //已使用
                dom5g:'dom-5g', //已过期

                dom50w:'dom-50w', //未使用
                dom50u:'dom-50u', //已使用
                dom50g:'dom-50g', //已过期

                dom5000w:'dom-5000w', //未使用
                dom5000u:'dom-5000u', //已使用
                dom5000g:'dom-5000g', //已过期

                dom6800w:'dom-6800w', //未使用
                dom6800u:'dom-6800u', //已使用
                dom6800g:'dom-6800g', //已过期

                dom800w:'dom-800w', //未使用
                dom800u:'dom-800u', //已使用
                dom800g:'dom-800g', //已过期

                domCpw:'dom-cpw', //未使用
                domCpu:'dom-cpu', //已使用
                domCpg:'dom-cpg', //已过期
            },
        };

        var opts = $.extend(defaults, options); // 合并接收对象数据

        var _publish = {
            /**
             * 模块初始化入口
             *
             * @param {string} opts 外部参数对象与内部默认参数对象合并后的对象
             */
            _init: function(opts){
                $.extend(true, this, opts); // 把合并后接收的对象，继续合并为需要返回的对象
                if(!_publish.utils.isLogin()){
                    location.href = "home.html"
                }
                _publish._initScale();
                quanService.initQuanList(_publish.activityId,function (json) {
                    _publish._initQuanDom(json);
                });
                this._initListeners();
            },
            /**
             * 自动获取dom元素
             *
             */
            _initDom:function () {
                for(var key in this.dom){
                    //带有子节点的dom
                    if(this.utils.isObject(this.dom[key])){
                        var el = this.element.querySelectorAll('['+this.dom[key]['el']+']');
                        var children = this.dom[key]['children'];
                        _publish.utils.each(el,function(dataI){
                            var attrDataI = $(dataI).attr('data-i');
                            _publish.dom[key][attrDataI] = _publish.dom[key][attrDataI] || {};
                            _publish.dom[key][attrDataI]['el'] = dataI;
                            _publish.utils.each(children,function(it,ky){
                                _publish.dom[key][attrDataI]['children'] = _publish.dom[key][attrDataI]['children']
                                    || {};
                                _publish.dom[key][attrDataI]['children'][ky] = dataI.querySelector('['+it+']');
                            });
                        });
                    }
                    //单节点
                    if(this.utils.isString(this.dom[key])){
                        var doms = this.element.querySelectorAll('['+this.dom[key]+']');
                        this.dom[key] = doms.length > 1 ? doms : doms[0];
                    }
                }
            },
            /**
             * 初始化dom样式
             *
             */
            _initDomStyle:function () {
                var scaleWidth = this.scaleWidth,
                    scaleHeight = this.scaleHeight;
                //初始化1800w样式
                $(_publish.dom.dom1800w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-18*scaleWidth+'px ',
                    backgroundPositionY:-10*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化1800u样式
                $(_publish.dom.dom1800u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-280*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化1800g样式
                $(_publish.dom.dom1800g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-8*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化20w样式
                $(_publish.dom.dom20w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-277*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化20u样式
                $(_publish.dom.dom20u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-555*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化20g样式
                $(_publish.dom.dom20g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-555*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化3800w样式
                $(_publish.dom.dom3800w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-1090*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化3800u样式
                $(_publish.dom.dom3800u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-820*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化3800g样式
                $(_publish.dom.dom3800g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-820*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化5w样式
                $(_publish.dom.dom5w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-1090*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化5u样式
                $(_publish.dom.dom5u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-1360*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化5g样式
                $(_publish.dom.dom5g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-1360*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化50w样式
                $(_publish.dom.dom50w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-1630*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化50u样式
                $(_publish.dom.dom50u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-1905*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化50g样式
                $(_publish.dom.dom50g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-1630*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化5000w样式
                $(_publish.dom.dom5000w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-1905*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化5000u样式
                $(_publish.dom.dom5000u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-2175*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化5000g样式
                $(_publish.dom.dom5000g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-2175*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化6800w样式
                $(_publish.dom.dom6800w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-15*scaleWidth+'px ',
                    backgroundPositionY:-2445*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化6800u样式
                $(_publish.dom.dom6800u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-8*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化6800g样式
                $(_publish.dom.dom6800g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-1945*scaleWidth+'px ',
                    backgroundPositionY:-2445*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化800w样式
                $(_publish.dom.dom800w).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-280*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化800u样式
                $(_publish.dom.dom800u).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-820*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化800g样式
                $(_publish.dom.dom800g).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-550*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });

                //初始化cpw样式
                $(_publish.dom.domCpw).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-1090*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化cpu样式
                $(_publish.dom.domCpu).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-1635*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
                //初始化cpg样式
                $(_publish.dom.domCpg).css({
                    width:scaleWidth * 1920 + 'px',
                    height:scaleHeight * 250 + 'px',
                    backgroundPositionX:-3875*scaleWidth+'px ',
                    backgroundPositionY:-1360*scaleHeight+'px',
                    backgroundSize:scaleWidth*6460+'px auto'
                });
            },
            /**
             * 初始化比例尺
             *
             */
            _initScale:function () {
                var mapWidth = this.element.clientWidth,
                    mapHeight = this.element.clientHeight;
                this.scaleWidth = mapWidth / 1920;
                this.scaleHeight = mapWidth / 1080;
            },
            _initQuanDom:function (quanList) {
                var strategy = {
                    '14':{
                        '1':'dom-cpw', //未使用
                        '0':'dom-cpu', //已使用
                        '-1':'dom-cpg', //已过期
                    },
                    '15':{
                        '1':'dom-3800w', //未使用
                        '0':'dom-3800u', //已使用
                        '-1':'dom-3800g', //已过期
                    },
                    '16':{
                        '1':'dom-1800w', //未使用
                        '0':'dom-1800u', //已使用
                        '-1':'dom-1800g', //已过期
                    },
                    '17':{
                        '1':'dom-20w', //未使用
                        '0':'dom-20u', //已使用
                        '-1':'dom-20g', //已过期
                    },
                    '18':{
                        '1':'dom-5000w', //未使用
                        '0':'dom-5000u', //已使用
                        '-1':'dom-5000g', //已过期
                    },
                    '19':{
                        '1':'dom-800w', //未使用
                        '0':'dom-800u', //已使用
                        '-1':'dom-800g', //已过期
                    },
                    '20':{
                        '1':'dom-50w', //未使用
                        '0':'dom-50u', //已使用
                        '-1':'dom-50g', //已过期
                    },
                    '21':{
                        '1':'dom-6800w', //未使用
                        '0':'dom-6800u', //已使用
                        '-1':'dom-6800g', //已过期
                    },
                    '23':{
                        '1':'dom-5w', //未使用
                        '0':'dom-5u', //已使用
                        '-1':'dom-5g', //已过期
                    },
                };
                _publish.utils.each(quanList,function (item) {
                    var code = item.attrId,
                        status = item.status;
                    var attrDom = strategy[code+''][status+''];
                    $(_publish.element).append($('<i '+attrDom+' class="sprite"></i>'));
                });
                this._initDom();
                _publish._initDomStyle();
            },
            /**
             * 渲染数据
             * @params json
             */
            _renderData:function (json) {

            },
            /**
             * 绑定事件监听器
             *
             */
            _initListeners: function(){

            },
            /**
             * 回调事件 外部调用
             *
             */
            callback: {

            },
            /**
             * 工具类事件
             *
             */
            utils: {
                delegates: function(element, configs){
                    var el = $(element);
                    for (var name in configs) {
                        var value = configs[name];
                        if (typeof value === 'function') {
                            var obj = {};
                            obj.click = value;
                            value = obj;
                        }
                        for (var type in value) {
                            el.delegate(name, type, value[type]);
                        }
                    }
                },
                each:function (obj,callback) {
                    //针对ie8
                    if(obj == '[object StaticNodeList]'){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                        return;
                    }
                    //针对正常浏览器
                    if(this.isArray(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isNodeList(obj)){
                        for(var i = 0; i < obj.length; i++){
                            callback(obj[i],i);
                        }
                    }
                    if(this.isObject(obj)){
                        for(var key in obj){
                            callback(obj[key],key);
                        }
                    }
                },
                isString:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object String]';
                },
                isArray:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Array]';
                },
                isNodeList:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object NodeList]';
                },
                isObject:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Object]';
                },
                isNull:function (obj) {
                    return Object.prototype.toString.call(obj) == '[object Null]';
                },
                isIe8:function () {
                    var browser=navigator.appName
                    var b_version=navigator.appVersion
                    var version=b_version.split(";");
                    var trim_Version=version[1].replace(/[ ]/g,"");
                    return browser=="Microsoft Internet Explorer" && trim_Version=="MSIE8.0";
                },
                //是否登录
                isLogin:function () {
                    if(!localStorage.token){
                        return false;
                    }
                    return true;
                },
                //是否正在旋转
                isRotating:function () {
                    return _publish.rotating;
                },
                //获取区间随机数
                randRange:function (n,m) {
                    return Math.floor(Math.random()*(m-n+1)+n);
                }
            }
        };
        _publish._init(opts);
        return _publish;
    }
    return _initPublish;
})();

window.onload = function () {
    var pageAwardQuan = new PageAwardQuan();
}