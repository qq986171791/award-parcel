/**
 * Created by user on 2018/3/12.
 */

export default function ($) {
    var jQuery = $;
    //备份jquery的ajax方法123
    var _ajax=$.ajax;

    window._url_webchart_api = "https://www.xingtown.com/wechat/xg-wechat/api/";
    window._url_webchart_page = "https://www.xingtown.com/wechat/xg-wechat/";

    //重写jquery的ajax方法
    $.ajax=function(opt){
        //备份opt中error和success方法
        var fn = {
            error:function(XMLHttpRequest, textStatus, errorThrown){},
            success:function(data, textStatus){}
        }
        // if(!opt.url)
        // {
        //     return false
        // }
        if(opt.url.indexOf(".json")==-1&&opt.url.indexOf("http")==-1)
        {
            //开发环境地址
            opt.url = "http://192.168.14.219:8080/" + opt.url;
            //测试环境地址
            //  opt.url = "https://www.tdenergys.com/crm-debug/td-crm-web/" + opt.url;
            //正式环境地址
            //opt.url = "https://www.xingtown.com/td-crm-web/" + opt.url;
        }
        if(!opt.type) {
            opt.type = "post";
        }
        if(!opt.dataType) {
            opt.dataType = "json";
        }

        if(opt.data&&!opt.myType&&(typeof  opt.data)=="object")
        {
            opt.data=JSON.stringify(opt.data);
        }
        // opt.crossDomain=true;

        if(opt.error){
            fn.error=opt.error;
        }
        if(opt.success){
            fn.success=opt.success;
        }

        if(!opt["clearHeaders"]) {
            opt.headers={
                'Content-Type': 'application/json',
                'x-auth-token':localStorage.token?localStorage.token:""
            }
        }

        //扩展增强处理
        var _opt = $.extend(opt,{
            error:function(XMLHttpRequest, textStatus, errorThrown){
                //错误方法增强处理

                fn.error(XMLHttpRequest, textStatus, errorThrown);
            },
            success:function(data, textStatus){
                //成功回调方法增强处理
                // if(null != data["status"] && 1001 == data["status"]) {
                //     window.top.location = "#/login";
                // }
                fn.success(data, textStatus);
            }
        });
        _ajax(_opt);
    };
}