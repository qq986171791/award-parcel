/**
 * Created by user on 2018/3/13.
 */


export default {
    each:function (obj,callback) {
        //针对ie8
        if(obj == '[object StaticNodeList]'){
            for(var i = 0; i < obj.length; i++){
                callback(obj[i],i);
            }
            return;
        }
        //针对正常浏览器
        if(this.isArray(obj)){
            for(var i = 0; i < obj.length; i++){
                callback(obj[i],i);
            }
        }
        if(this.isNodeList(obj)){
            for(var i = 0; i < obj.length; i++){
                callback(obj[i],i);
            }
        }
        if(this.isObject(obj)){
            for(var key in obj){
                callback(obj[key],key);
            }
        }
    },
    isString:function (obj) {
        return Object.prototype.toString.call(obj) == '[object String]';
    },
    isArray:function (obj) {
        return Object.prototype.toString.call(obj) == '[object Array]';
    },
    isNodeList:function (obj) {
        return Object.prototype.toString.call(obj) == '[object NodeList]';
    },
    isObject:function (obj) {
        return Object.prototype.toString.call(obj) == '[object Object]';
    },
    isNull:function (obj) {
        return Object.prototype.toString.call(obj) == '[object Null]';
    },
    getQueryString:function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);  //获取url中"?"符后的字符串并正则匹配
        var context = "";
        if (r != null)
            context = r[2];
        reg = null;
        r = null;
        return context == null || context == "" || context == "undefined" ? "" : context;
    },
    encodeJson(json){
        let data = json
        data = JSON.stringify(data)
        data = encodeURIComponent(data)
        data = btoa(data)
        return data;
    },
    decodeJson(json){
        let data = json;
        data = atob(data);
        data = decodeURIComponent(data);
        data = JSON.parse(data);
        return data;
    }
}