/**
 * Created by user on 2018/3/13.
 */

export default {
    activityId:7,
    c(key,val){
        if(!val){
            return this[key];
        }
        return this[key] = val;
    }
}