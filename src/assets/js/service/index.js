const $ = require('jquery');
import Auth from '../../lib/auth';
import config from '../config';
import utils from '../utils'
Auth($);
export var userService = (function () {
    var url = {
        getUserInfo:'rest/user/getUserInfo'
    };
    var _user = null;
    return {
        initUserInfo:function (callback) {
            $.post(url.getUserInfo,{},function (json) {
                if(1 == json.status){
                    _user = json.result.user;
                    callback(json);
                }
            });
        },
        setUserInfo:function (user) {
        },
        getUserInfo:function () {
            return _user;
        }
    };
})();
export var wxService = (function () {
    (function () {
        //获取config配置的参数
        $.ajax({
            url: _url_webchart_api +"jsapi?url=" + encodeURIComponent(window.location.href),
            type: 'GET',
            clearHeaders: true,
            async:false,
            success: function (data) {
                //  通过config接口注入权限验证配置
                wx.config({
                    debug: false,
                    appId: data.appid,
                    timestamp: data.timeStamp,
                    nonceStr: data.nonceStr,
                    signature: data.signature,
                    jsApiList: ['onMenuShareTimeline','onMenuShareAppMessage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                });
                wx.ready(function(){
                    wx.checkJsApi({
                        jsApiList: [
                            'onMenuShareTimeline',
                            'onMenuShareAppMessage'
                        ],
                        success: function (res) {
                        }
                    });
                    wx.error(function (resp) {
                    });
                });
            }
        });
    })();
    return {
        initShare:function (cfg) {
            wx.ready(function () {
                //注册分享到朋友圈
                wx.onMenuShareTimeline({
                    title: cfg.title,
                    link: cfg.link+'?recommendUserId='+cfg.recommendUserId,
                    imgUrl: cfg.imgUrl, // 分享图标
                    success: cfg.success,
                    cancel: cfg.cancel
                });
                //注册分享给朋友
                wx.onMenuShareAppMessage({
                    title: cfg.title,
                    link: cfg.link+'?recommendUserId='+cfg.recommendUserId,
                    imgUrl: cfg.imgUrl, // 分享图标
                    success: cfg.success,
                    cancel: cfg.cancel,
                    desc: '星光之家十一抽奖活动', // 分享描述
                    type: '',
                    dataUrl: '',
                });
            });
        }
    }
})();
export var awardService = (function () {
    return {
        addAwardTimes:function (type) {
            $.post('/rest/record/save',{activityId:config.activityId,source:type,count:1},function(json){
                location.reload();
            });
        }
    };
})();
export var activityService = (function () {
    var activityId = 7;
    var url = {
        getActivityInfo:'rest/activity/getInfo'
    };
    var activityiInfo = null;


    return {
        initActivityInfo(callback){
            if(this.utils.isPreview()){
                let json = utils.decodeJson(utils.getQueryString('activityiInfo'));
                activityiInfo = json;
                callback(json);
                return;
            }
            $.post(url.getActivityInfo,{id:activityId},(json) => {
                activityiInfo = json;
                callback(json);
            });
        },
        getActivityInfo:function () {
            return activityiInfo;
        },
        utils:{
            isPreview(){
                return utils.getQueryString('activityiInfo');
            }
        }
    };
})();